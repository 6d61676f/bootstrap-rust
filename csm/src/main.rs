#[macro_use]
extern crate log;
extern crate env_logger;

use std::net::UdpSocket;
use std::thread;
use std::sync::{Arc, Mutex};
use std::io::ErrorKind;

fn main() {
    env_logger::init();
    let socket = Arc::new(Mutex::new(UdpSocket::bind("127.0.0.1:5050").unwrap()));
    let mut buffer = Box::new([0; 256]);
    let mut buffer2;
    let done = Arc::new(Mutex::new(false));
    {
        socket.lock().unwrap().set_nonblocking(true).unwrap();
    }
    let mut threads = vec![];
    loop {
        match socket.lock().unwrap().recv_from(&mut buffer[..]) {
            Ok((recv_len, recv_from)) => {
                let t_sock = socket.clone();
                let t_done = done.clone();
                buffer2 = buffer.clone();
                let t = thread::spawn(move || {
                    debug!("spawned...");
                    let t_sock = t_sock.lock().unwrap();
                    let rez = String::from_utf8(buffer2[..recv_len].to_vec()).unwrap();
                    if rez.trim().eq_ignore_ascii_case("stop") {
                        t_sock.send_to(b"stopping", recv_from).unwrap();
                        *t_done.lock().unwrap() = true;
                    } else {
                        t_sock.send_to(&buffer2[..recv_len], recv_from).unwrap();
                    }
                    info!("sent response to {:#?}", recv_from);
                });
                threads.push(t);
            }
            Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
                trace!("Would have blocked...sleeping");
            }
            Err(e) => panic!("{:#?}", e),
        }
        if *done.lock().unwrap() == true {
            info!("breaking the waves...");
            break;
        }
    }
    for i in threads {
        i.join().unwrap();
    }
}
