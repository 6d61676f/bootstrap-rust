//! # This is how you document a crate
//! ## With `//!`

//Re-export -- access the enum directly
pub use moloz::NonColors;

pub mod moloz {
    //! ## Now i'm documenting a module
    //! ### Yey

    /// I can document a function like so
    ///
    /// Also run code from comments...
    /// # Examples
    ///
    /// ```
    /// let v1 : [i32 ; 3] = [1, 2, 3];
    /// let v2 = libby::moloz::useless_shit(&v1);
    /// assert_eq!(v1,v2);
    /// ```
    pub fn useless_shit<T: Clone>(arg1 : &T) -> T {
        let temp = arg1.clone();
        temp
    }

    pub enum NonColors {
        Black,
        White
    }
}

#[cfg(test)]
mod tests {
    use super::moloz::useless_shit;

    #[test]
    fn test_useless() {
        let v1 = vec![1,2,3];
        let v2 = useless_shit(&v1);
        assert_eq!(v1, v2);
    }
}
