#[macro_use]
extern crate serde_derive;
extern crate bincode;
extern crate tokio;

use tokio::prelude::{Async, Future};

#[derive(Serialize, Deserialize, PartialEq, Debug)]
enum VAL {
    W0l0l0,
    Something(usize),
    Mofos(Vec<u8>),
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Data {
    vals: Vec<VAL>,
}

struct MyServer {
    sock: tokio::net::UdpSocket,
    events: Vec<Response>,
    count: usize,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Response;

impl tokio::prelude::Future for Response {
    type Item = ();
    type Error = ();
    fn poll(&mut self) -> Result<Async<()>, ()> {
        println!("I'm cool here, thank you");
        Ok(Async::Ready(()))
    }
}


impl MyServer {
    pub fn new(addr: &str, count: usize) -> MyServer {
        MyServer {
            sock: tokio::net::UdpSocket::bind(&addr.parse().unwrap()).unwrap(),
            events: vec![],
            count,
        }
    }
    pub fn read(&mut self, mut arr: &mut Vec<u8>) -> Result<Async<()>, std::io::Error> {
        match self.sock.poll_recv_from(&mut arr)? {
            Async::Ready(_) => {
                self.events.push(Response {});
                return Ok(Async::Ready(()));
            }
            Async::NotReady => {
                return Ok(Async::NotReady);
            }
        }
    }
}

impl Future for MyServer {
    type Item = ();
    type Error = ();
    fn poll(&mut self) -> Result<Async<()>, ()> {
        loop {
            let mut arr: Vec<u8> = vec![];
            match self.read(&mut arr).unwrap() {
                Async::Ready(_) => println!("Recevived stuff"),
                Async::NotReady => break,
            }
        }

        for i in self.events.pop() {
            tokio::spawn(i);
            if self.count > 0 {
                self.count -= 1;
            }
        }

        if self.count == 0 {
            return Ok(Async::Ready(()));
        }
        Ok(Async::NotReady)
    }
}

fn server(addr: &str) {
    let fm = MyServer::new(addr, 10);
    tokio::run(fm);
}

fn client(addr: &str) {
    let mut data = Data { vals: vec![] };
    data.vals.push(VAL::W0l0l0);
    data.vals.push(VAL::Something(10));
    data.vals.push(VAL::Mofos(vec![10, 11, 12]));
    let serial = bincode::serialize(&data).unwrap();

    let caddr: std::net::SocketAddr = "0.0.0.0:0".parse().unwrap();
    let sock = std::net::UdpSocket::bind(&caddr).unwrap();
    let saddr: std::net::SocketAddr = addr.parse().unwrap();

    for _ in 0..10 {
        sock.send_to(&serial, &saddr).unwrap();
        std::thread::sleep(std::time::Duration::from_millis(500));
        println!("Client sent data");
    }
}

pub fn help() {
    println!("prog addr:port [server|client]");
    std::process::exit(1);
}

fn main() {
    let mut args: Vec<String> = std::env::args().collect();
    if let (Some(t), Some(addr)) = (args.pop(), args.pop()) {
        if t.to_lowercase() == "client" {
            client(&addr);
        } else if t.to_lowercase() == "server" {
            server(&addr);
        } else {
            help();
        }
    } else {
        help();
    }
}
