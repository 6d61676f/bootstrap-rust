trait PrettyPrint {
    fn pprint(&self) -> String;
    fn def_func(&self) -> () {
        println!("carry on...");
    }
}

struct Student {
    name: String,
    age: u8,
    score: f32,
}

struct Book {
    title: String,
    author: String,
    publisher: String,
}

impl PrettyPrint for Student {
    fn pprint(&self) -> String {
        format!(
            "Student {} , of age {}, had score {}",
            self.name,
            self.age,
            self.score,
        )
    }
}

impl PrettyPrint for Book {
    fn pprint(&self) -> String {
        format!(
            "Book {}, written by {}, has been published by {}",
            self.title,
            self.author,
            self.publisher
        )
    }
}

//Trait Bounds
fn print_shit<T: PrettyPrint>(item: &T) {
    println!("{}", item.pprint());
}


fn main() {
    let pesca = Student {
        name: String::from("Pescarus"),
        age: 24,
        score: 10.0,
    };
    let pata = Book {
        title: String::from("In Patagonia"),
        author: String::from("Bruce Chatwin"),
        publisher: String::from("Dunno, lol"),
    };
    print_shit(&pesca);
    print_shit(&pata);
}
