use std::ops::Deref;
use std::fmt::Debug;
use std::rc::Rc;
use std::cell::RefCell;

enum List<T> {
    Cons(T, Box<List<T>>),
    Nil,
}

use List::{Cons, Nil};

///our own data type
#[derive(Debug)]
pub struct MyPtr<T: Debug>(T);

///allocator
impl<T: Debug> MyPtr<T> {
    pub fn new(x: T) -> MyPtr<T> {
        MyPtr(x)
    }
}

/// the '*' operator for accessing the type
impl<T: Debug> Deref for MyPtr<T> {
    type Target = T;
    fn deref(&self) -> &T {
        &self.0
    }
}

/// the destructor called at out-of-scope
impl<T: Debug> Drop for MyPtr<T> {
    fn drop(&mut self) {
        println!("Deallocating memory for {:?}....", self);
    }
}

///Function that takes a reference to a generic type
fn print_my_ptr<T: Debug>(val: &T) {
    println!("yeey {:?}", val);
}

trait TakeExam {
    fn take<'a>(&'a self, course: &'a str) -> usize;
}

struct Pupil<'a, T: 'a + TakeExam> {
    stud: &'a T,
    average: usize,
}

struct MockPupil {
    //Even though a MockPupil objecy would be passed
    //  as immutable reference, we can mutate the data in it using
    //  RefCell. `borrow` and `borrow_mut()` returns mutable and immutable
    //  references to data Vec kept within.
    grades: RefCell<Vec<(String, usize)>>,
}

impl MockPupil {
    pub fn new() -> MockPupil {
        MockPupil { grades: RefCell::new(Vec::new()) }
    }
}

impl TakeExam for MockPupil {
    fn take(&self, course: &str) -> usize {
        //self if an immutable borrow, but we can
        //  mutate it's state by using a RefCell
        //  Reference Mutability Pattern
        //This is evaluated at runtime...
        self.grades.borrow_mut().push((String::from(course), 5));

        // This would cause a panic because of reference rules
        //let mut _m1 = self.grades.borrow_mut();
        //let _m2 = self.grades.borrow();
        5
    }
}



///
/// Rc<T> enables multiple owners of the same data; Box<T> and RefCell<T> have single owners.
/// Box<T> allows immutable or mutable borrows checked at compile time;
///     Rc<T> only allows immutable borrows checked at compile time;
///     RefCell<T> allows immutable or mutable borrows checked at runtime.
/// Because RefCell<T> allows mutable borrows checked at runtime,
///     we can mutate the value inside the RefCell<T> even when the RefCell<T> is immutable.
///
fn main() {
    let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
    print_hell(&list);
    let ptr = MyPtr(5);
    println!("ptr holds {}", *ptr);
    //deref coercion
    //struct implements Deref traits thus allows us
    //to take a reference directly to the type kept in the
    //structure

    //we can explicitly drop a a pointer by calling `std::mem::drop`
    //std::mem::drop(ptr);
    print_my_ptr(&ptr);

    //Build In Smart Pointers
    let b = Box::new(String::from("Simple single owned pointer"));
    print_my_ptr(&b);

    //Only for single threaded applications...
    let c_1 = Rc::new(String::from("Reference counting pointer"));
    let _c_2 = Rc::clone(&c_1);
    println!("reference count {}", Rc::strong_count(&c_1));
    {
        let c_3 = Rc::clone(&c_1);
        println!("reference count {}", Rc::strong_count(&c_1));
    }

    //RefCell
    let mo = Pupil {
        stud: &MockPupil::new(),
        average: 10,
    };
    mo.stud.take("raki taki");
}

fn print_hell<T>(x: &List<T>)
where
    T: std::fmt::Debug,
{
    match x {
        &Cons(ref i, ref j) => {
            print_hell(j);
            println!("{:#?}", i);
        }
        &Nil => (),
    }
}
