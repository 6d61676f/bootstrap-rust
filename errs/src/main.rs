use std::fs::File;
use std::io::ErrorKind;
use std::io::Error;
use std::io::Read;

fn main() {
    //let _fis = match File::open("foo.txt") {
    //    Ok(f) => f,
    //    Err(ref err) if err.kind() == ErrorKind::NotFound => {
    //        match File::create("foo.txt") {
    //            Ok(nou) => nou,
    //            Err(errnou) => panic!("Caught {:?}", errnou),
    //        }
    //    }
    //    Err(err) => {
    //        println!("Caught {:#?}", err);
    //        return ();
    //    }
    //};
    ////or
    //let _fis = File::open("foo.txt").expect("Could not open foo.txt");
    ////or
    //let _fis = File::open("foo.txt").unwrap();
    let _rez = match propagate("/etc/fstab") {
        Ok(s) => {
            println!("{}", s);
            s
        }
        Err(s) => {
            println!("{:#?}", s);
            "".to_string()
        }
    };
    match sneaky_propagate("/etc/motd") {
        Ok(s) => println!("{}", s),
        Err(e) => println!("{:#?}", e),
    };
}

fn propagate(fis: &str) -> Result<String, Error> {
    let mut f = match File::open(fis) {
        Ok(f) => f,
        Err(err) => return Err(err),
    };

    let mut s = String::new();

    let ret = match f.read_to_string(&mut s) {
        Ok(read_size) => {
            println!("read {} from {:#?}", read_size, f);
            Ok(s)
        }
        Err(err) => Err(err),
    };
    return ret;
}

//NOTE
//'?' is the propagate operator
//If an error appears it is returned immediately,
//else execution continues...
fn sneaky_propagate(fis: &str) -> Result<String, Error> {
    let mut s: String = String::new();
    File::open(fis)?.read_to_string(&mut s)?;
    return Ok(s);
}
