fn args(x: i32, y: String) {
    println!("{} and {}", x, y);
    //5 -- error - function returns () -- voidish
}

fn main() {
    println!("{}", ceva());
    let tuple = (5, 4, ceva());
    //let (a,b,c) = tuple; -- invalidates tuple...
    args(2, ceva());
    println!("{} {} {}", tuple.0, tuple.1, tuple.2);
    println!("{} {}", is_five(10), is_five(5));
    //ternary exprs
    let ok = if is_five(10) {
        "True".to_string()
    } else {
        "False".to_string()
    };
    println!("{}", ok);
}

fn ceva() -> String {
    //Trailing return type - like c++ lambda expressions
    let var: String = "Salut".to_string(); //Statement -- ';' -- no returning type
    var //Expressions -- no ';'
    //or
    //return var;
}

fn is_five(x: u8) -> bool {
    let ret: bool; //notice that we do not need mutable here
    //BECAUSE WE DID NOT INITIALIZE THE VARIABLE YET
    if x == 5 {
        ret = true;
    } else {
        ret = false;
    }
    ret // or return ret;
}
