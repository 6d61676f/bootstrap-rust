#[macro_use]
extern crate log;
extern crate env_logger;
extern crate getopts;

use std::fs;
use getopts::Options;
//use std::sync::{Arc, Mutex};

enum _Recurse {
    Yes,
    No,
    Depth(u32),
}

//Hacking Art of Exploitation
fn hex_dump(vals: &[u8], size: usize) {
    for i in 0..size {
        let mut byte = vals[i];
        print!("{:2X} ", byte);
        if i % 16 == 15 || i == size - 1 {
            for _ in 0..15 - (i % 16) {
                print!("{:3}", ' ');
            }
            print!("| ");
            for j in (i - (i % 16))..i + 1 {
                let byte = vals[j] as char;
                if byte.is_alphanumeric() {
                    print!("{} ", byte);
                } else {
                    print!(". ");
                }
            }
            println!(" |");
        }
    }
}

fn print_file(path: &str) -> Result<(), std::io::Error> {
    if let Ok(file) = fs::read(&path) {
        print!("\n\t{}:\n", path);
        if let Ok(string_file) = String::from_utf8(file.clone()) {
            println!("\'{}\'", string_file);
        } else {
            hex_dump(&file, file.len());
        }
        println!("");
    }
    Ok(())
}

fn print_dir(path: &str, recurse: bool, list_file: bool) -> Result<(), std::io::Error> {
    if let Ok(dir) = fs::read_dir(&path) {
        for entry in dir {
            if let Ok(entry) = entry {
                info!("Obtained valid DirEntry {:?}", entry);
                if let Some(ref name) = entry.path().to_str() {
                    if let Ok(entry_type) = entry.file_type() {
                        println!("{}", name);
                        if entry_type.is_dir() {
                            info!("about to recurse into {}", name);
                            if recurse {
                                print_dir(&name, recurse, list_file)?;
                            }
                        } else if entry_type.is_file() {
                            info!("about to print file {}", name);
                            if list_file {
                                print_file(&name)?;
                            }
                        }
                    }
                }
            }
        }
    }
    Ok(())
}

fn print_usage(exec: &str, opt: Options) {
    let form = format!("USAGE: {} FILE [options]", exec);
    print!("{}", opt.usage(&form));
}

fn main() {
    env_logger::init().unwrap();

    let mut opts = Options::new();
    opts.optflag("r", "recurse", "Recurse into directories");
    opts.optflag("p", "print", "Print file to terminal");
    opts.optflag("h", "help", "Print this useless help");

    let args: Vec<String> = std::env::args().collect();
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(e) => panic!("{}", e.to_string()),
    };

    if matches.opt_present("h") {
        print_usage(&args[0], opts);
        std::process::exit(1);
    }
    let recurse = matches.opt_present("r");
    let print = matches.opt_present("p");
    let input = if matches.free.is_empty() {
        print_usage(&args[0], opts);
        std::process::exit(1);
    } else {
        matches.free[0].clone()
    };


    if args.len() < 2 {
        print_usage(&args[0], opts);
        std::process::exit(1);
    } else {
        let _ = match fs::metadata(&args[1]) {
            Ok(ref file) if file.is_dir() => print_dir(&input, recurse, print),
            Ok(ref file) if file.is_file() => print_file(&input),
            Ok(e) => {
                panic!("{:#?}", e);
            }
            Err(e) => {
                print_usage(&args[0], opts);
                Err(e)
            }
        };
    }
}
