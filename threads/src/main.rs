use std::thread;
use std::time::Duration;
use std::sync::{Mutex, mpsc, Arc};

fn main() {
    //channels();
    simple_threads();
    simple_mutex();
    mutex_count();
}

fn mutex_count() {
    let counter = Arc::new(Mutex::new(0));
    let mut threads = vec![];

    for _ in 1..10 {
        let _c1 = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut val = _c1.lock().unwrap();
            *val += 1;
            println!("{:?}:{}", thread::current().id(), *val);
        });
        threads.push(handle);
    }

    for t in threads {
        t.join().unwrap();
    }

    println!("final value is {}", *counter.lock().unwrap());
}

fn simple_mutex() {
    let m = Mutex::new(69);
    {
        let mut val = m.lock().unwrap();
        *val += 1;
    }
    println!("{}", *m.lock().unwrap());
}

fn channels() {

    let mut threads = vec![];

    //channels
    //multiple producer, single consumer
    let (tx, rx) = mpsc::channel();

    //the endpoints of the channel must be owned
    //  by a certain context in order to guarantee ownership of sent items
    //the multiple part
    let tx1 = mpsc::Sender::clone(&tx);

    let t1 = thread::spawn(move || {
        let val = vec![1, 2, 3, 4, 5];
        for i in &val {
            //is this ok?
            tx.send(format!("{:?}:{}", thread::current().id(), *i))
                .unwrap();
        }
    });

    threads.push(t1);

    //while let Ok(i) = rx.recv() {
    //println!("{:?}", i);
    //}
    ////or

    let t2 = thread::spawn(move || for i in rx {
        println!("{:?}", i);
    });
    threads.push(t2);

    let t3 = thread::spawn(move || { tx1.send(String::from("hi")).unwrap(); });

    threads.push(t3);

    for i in threads {
        i.join().unwrap();
    }
}

fn simple_threads() {

    let vals = vec![1, 2, 3, 4, 5];
    let mut joins = vec![];

    let printy = || for i in 1..10 {
        thread::sleep(Duration::from_millis(i));
        println!("slept {} miliseconds", i);
    };

    // threads can only spawn closures
    let t1 = thread::spawn(printy);
    // we cannot use closures that need arguments
    // we have to put the closure inside another closure and pass the args
    let t2 = thread::spawn(move || {
        for i in vals {
            print!("{} ", i);
        }
        print!("\n");
    });

    // would not compile
    //drop(vals);

    joins.push(t1.join());
    joins.push(t2.join());

    for _j in joins {
        println!("{:?}", _j);
    }

}
