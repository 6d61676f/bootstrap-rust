//Check
//https://doc.rust-lang.org/book/first-edition/macros.html

use std::error::Error;

macro_rules! p_error {
    ( $( $x:expr ),* ) => {
        {
            use std::io::{Error,ErrorKind};
            let mut msg = String::new();
            $(
                msg.push_str(&format!("{:?} ", $x));
             )*
            let err = Box::new(Error::new(
                    ErrorKind::InvalidData,
                    format!("{}:{} -> {}", file!(), line!(), msg)
                    ));
            return Err(err);
        }
    };
}


fn main() -> Result<(), Box<Error>> {
    p_error!("Wtf main!");
}
