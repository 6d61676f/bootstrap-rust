extern crate rand;

use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    let secret = rand::thread_rng().gen_range(0, 100);
    let mut osrng = rand::os::OsRng::new().expect("Fak");
    let ceva = osrng.next_u32();
    println!("osnrg e {}", ceva);

    loop {
        let mut guess_str = String::new();

        println!("Guess the number! 0-100");
        io::stdin().read_line(&mut guess_str).expect(
            "Read has failed",
        );
        let guess: i32 = match guess_str.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };


        match guess.cmp(&secret) {
            Ordering::Less => println!("You guessed Less"),
            Ordering::Greater => println!("You guessed Greater"),
            Ordering::Equal => {
                println!("Equal!");
                break;
            }
        }
        println!("You guessed {} and the secret was {}", guess, secret);
    }
}
