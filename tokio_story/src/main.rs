extern crate tokio;

use tokio::net::UdpSocket;
use tokio::prelude::*;
use tokio::io::Error;
use std::net::SocketAddr;

struct MyFuture {
    sock: UdpSocket,
    to: Vec<(SocketAddr, Vec<u8>)>,
    count: usize,
}

impl MyFuture {
    pub fn new(addr: &str, count: usize) -> MyFuture {
        MyFuture {
            sock: UdpSocket::bind(&addr.parse().unwrap()).unwrap(),
            to: vec![],
            count,
        }
    }
    pub fn read(&mut self, buffer: &mut [u8; 1024]) -> Result<Async<(usize, SocketAddr)>, Error> {
        match self.sock.poll_recv_from(&mut buffer[..])? {
            Async::Ready((ref len, ref addr)) => {
                self.to.push((*addr, buffer[0..*len].to_vec()));
                Ok(Async::Ready((*len, *addr)))
            }
            Async::NotReady => Ok(Async::NotReady),
        }
    }
    pub fn write_all(&mut self) -> Result<Async<()>, Error> {
        let index = self.to.len();
        for i in (0..index).rev() {
            match self.sock.poll_send_to(&self.to[i].1, &self.to[i].0)? {
                Async::Ready(size) => {
                    println!("sent {} bytes back to {:?}", size, self.to[i].0);
                    self.to.remove(i);
                }
                Async::NotReady => {
                    return Ok(Async::NotReady);
                }
            }
        }
        return Ok(Async::Ready(()));
    }
}

impl Future for MyFuture {
    type Item = ();
    type Error = ();
    fn poll(&mut self) -> Result<Async<()>, ()> {
        let mut arr = [0u8; 1024];
        loop {
            match self.read(&mut arr).unwrap() {
                Async::Ready((ref mut size, ref mut from)) => {
                    println!(
                        "Received {} bytes from {:?}:{:?}",
                        size,
                        from,
                        arr[0..*size].to_vec()
                    );
                }
                Async::NotReady => {
                    break;
                }
            }
        }
        match self.write_all().unwrap() {
            Async::Ready(_) => {
                self.count -= 1;
            }
            _ => {}
        }

        if self.count == 0 {
            return Ok(Async::Ready(()));
        } else {
            return Ok(Async::NotReady);
        }
    }
}

pub fn main() {
    let mf = MyFuture::new("127.0.0.1:5555", 5);
    tokio::run(mf);
}
