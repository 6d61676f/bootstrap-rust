fn main() {
    //for x in 0..5 {
    //loopy(x);
    //}
    for _j in 0..100 {
        for x in 1..90 {
            println!("{}th fibbo is {}", x, fibbo(x));
        }
    }
}

fn fibbo(x: u8) -> usize {
    if x < 3 {
        return 1;
    } else {
        let mut a: usize = 1;
        let mut b: usize = 1;
        let mut c: usize = 1;
        for _i in 3..(x + 1) {
            c = a + b;
            a = b;
            b = c;
        }
        return c;
    }
}

fn loopy(x: u8) {
    let mut i = x;
    match x {
        0 => {
            println!("FOR");
            for i in x..10 {
                print!("{} ", i);
            }
        }
        1 => {
            println!("WHILE");
            while i < 10 {
                print!("{} ", i);
                i = i + 1;
            }
        }
        2 => {
            println!("LOOP"); //loop if infinite at its roots -- must break out of it
            loop {
                if i == 10 {
                    break;
                } else {
                    print!("{} ", i);
                    i = i + 1;
                }
            }
        }
        _ => {
            println!("Something else");
        }
    }
    println!("");
}
