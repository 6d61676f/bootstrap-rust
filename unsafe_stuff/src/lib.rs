#[cfg(test)]
mod raw_ptrs {

    static mut VALUE: u32 = 10;

    #[test]
    fn test_raw_u8() {
        let mut num = 5u8;
        let initial = num;
        // Not ok, no compile
        //let r1 = &num as *const [u8;2];
        //let r2 = &mut num as *mut i32;
        let r3 = &num as *const u8;
        let r4 = &mut num as *mut u8;
        unsafe {
            //*r2 = 10;
            *r4 *= 2;
            assert_eq!(*r3, *r4);
        }
        assert_eq!(initial *2, num);
    }

    #[test]
    fn test_deref_null() {
        let mut _addr = 0x0usize as *mut usize;
        unsafe {
            // Would segfault...
            //*addr = 0xdeadbeef;
        }
    }

    #[test]
    fn write_in_global() {
        let val: u32 = 50;
        unsafe {
            VALUE = val;
            assert_eq!(val, VALUE);
        }
    }
}
