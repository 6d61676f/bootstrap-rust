use std::env;
use std::fs;
use std::io;
use std::io::Read;

#[derive(Debug)]
pub struct Config {
    filename: String,
    pattern: String,
    case_insensitive: bool,
}

impl Config {
    pub fn new() -> Result<Config, io::Error> {
        get_config()
    }
    pub fn get_filename<'a>(&'a self) -> &'a str {
        return &self.filename;
    }
    pub fn get_pattern<'a>(&'a self) -> &'a str {
        //lifetime specifier not needed because it default to self..
        return &self.pattern;
    }
    pub fn is_case_insensitive<'a>(&'a self) -> &'a bool {
        //lifetime specifier not needed because it default to self..
        return &self.case_insensitive;
    }
    pub fn read_file(&self) -> Result<String, io::Error> {
        if self.filename.len() <= 1 {
            Err(io::Error::new(
                io::ErrorKind::Other,
                "invalid number of args",
            ))
        } else {
            let mut _content = String::new();
            fs::File::open(&self.filename)?.read_to_string(
                &mut _content,
            )?;
            Ok(_content)
        }
    }
}

#[derive(Debug)]
pub struct Entry<'a>(pub usize, pub &'a str);

impl<'a> PartialEq for Entry<'a> {
    fn eq(&self, other: &Entry) -> bool {
        return self.0 == other.0 && self.1 == other.1;
    }

    fn ne(&self, other: &Entry) -> bool {
        return self.0 != other.0 || self.1 != other.1;
    }
}

pub fn help() {
    println!(
        "{} {} {}",
        env::args().nth(0).unwrap(),
        "filename",
        "pattern"
    );
}

fn get_config() -> Result<Config, io::Error> {
    if env::args().len() != 3 {
        return Err(io::Error::new(
            io::ErrorKind::InvalidData,
            "invalid # of args",
        ));
    } else {
        let _filename = env::args().nth(1).unwrap();
        let _pattern = env::args().nth(2).unwrap();
        let _ci = env::var("CI").is_ok();
        return Ok(Config {
            filename: _filename,
            pattern: _pattern,
            case_insensitive: _ci,
        });
    }
}

pub fn contains(needle: &str, haystack: &str) -> bool {
    for line in haystack.lines() {
        if line.contains(needle) {
            return true;
        }
    }
    return false;
}

pub fn search<'a>(needle: &'a str, haystack: &'a str) -> Vec<Entry<'a>> {
    let mut vals = Vec::new();
    for (i, line) in haystack.lines().enumerate() {
        if line.contains(needle) {
            vals.push(Entry(i, line));
        }
    }
    vals
}

pub fn search_ci<'a>(needle: &'a str, haystack: &'a str) -> Vec<Entry<'a>> {
    let mut vals = Vec::new();
    let needle = needle.to_lowercase();
    for (i, line) in haystack.lines().enumerate() {
        if line.to_lowercase().contains(&needle) {
            vals.push(Entry(i, line));
        }
    }
    vals
}
