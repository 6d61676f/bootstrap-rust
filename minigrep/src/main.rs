extern crate minigrep;
use minigrep::util;

use std::process;
use std::error::Error;

fn main() {
    if let Err(e) = greppy() {
        eprintln!("{:#?}", e);
        util::help();
        process::exit(1);
    };
}

//Box is a generic object saying that we
//return any type that implements the
//Error trait
fn greppy() -> Result<(), Box<Error>> {
    let _cfg = util::Config::new()?;
    let _content = _cfg.read_file()?;
    let _rez = if *_cfg.is_case_insensitive() {
        util::search_ci(_cfg.get_pattern(), &_content)
    } else {
        util::search(_cfg.get_pattern(), &_content)
    };
    for ref entry in _rez.iter() {
        println!("{}:{}", entry.0, entry.1);
    }
    Ok(())
}
