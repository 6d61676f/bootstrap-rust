use util::*;

#[test]
fn is_needle_in_haystack() {
    let needle = "raki";
    let haystack = "tanana tanana tanaraki";
    assert_eq!(contains(needle, haystack), true);
}

#[test]
fn where_is_needle() {
    let needle = "va";
    let haystack = "\
Lore ipsum ceva ceva,
Moloz, i h8 my life,
Something else,
altceva.";
    assert_eq!(
        search(needle, haystack),
        vec![Entry(0, "Lore ipsum ceva ceva,"), Entry(3, "altceva.")]
    );
}

#[test]
fn where_is_needle_ci() {
    let needle = "Va";
    let haystack = "\
Lore ipsum ceva ceva,
Moloz, i h8 my life,
Something else,
altcevA.";
    assert_eq!(
        search_ci(needle, haystack),
        vec![Entry(0, "Lore ipsum ceva ceva,"), Entry(3, "altcevA.")]
    );
}

#[test]
fn where_is_needle_cs() {
    let needle = "VA";
    let haystack = "\
Lore ipsum ceva ceva,
Moloz, i h8 my life,
Something else,
altceva.";
    assert_ne!(
        search(needle, haystack),
        vec![Entry(0, "Lore ipsum ceva ceva,"), Entry(3, "altceva.")]
    );
}
