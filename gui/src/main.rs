extern crate gui;
use gui::component::*;

pub fn main() {
    let mut s = Screen::new();
    s.add(Box::new(Window::new()));
    s.run();
}
