pub mod component {
    pub trait Draw {
        fn draw(&self);
    }
    pub struct Screen {
        pub components: Vec<Box<Draw>>,
    }
    impl Screen {
        pub fn run(&self) {
            for i in self.components.iter() {
                i.draw();
            }
        }
        pub fn add(&mut self, item: Box<Draw>) {
            self.components.push(item);
        }
        pub fn new() -> Screen {
            Screen { components: vec![] }
        }
    }
    pub struct Window {
        pub width: u32,
        pub height: u32,
        pub title: String,
    }
    impl Window {
        pub fn new() -> Window {
            Window {
                width: 1366,
                height: 768,
                title: String::from("Hello"),
            }
        }
    }
    impl Draw for Window {
        fn draw(&self) {
            println!(
                "Drawing a window {}x{} named \"{}\"",
                self.width,
                self.height,
                self.title
            );
        }
    }
}
