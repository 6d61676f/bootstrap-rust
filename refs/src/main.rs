#[derive(Debug)]
struct Entry {
    name: String,
    definition: String,
    dict: String,
    key: i64,
}

#[derive(Debug)]
enum Types {
    Synonim(String),
    Antonym,
}

fn main() {
    let a = Entry {
        name: String::from("Sal"),
        definition: String::from("salsa"),
        dict: String::from("ro"),
        key: 10,
    };
    let b: &Entry = &a;
    println!("{:#?} , {:#?} and {}", a, b, a.key_add());
    let b = Types::Antonym;
    let c = Types::Synonim("Partial".to_string());
    matchy(&b);
    matchy(&c);
    matchy_2(b);
    let c = "faLse";
    println!("c is {:?}", check_em(c));
    //println!("{:?}", b); //cannot use b after move
}

fn check_em(a: &str) -> Option<bool> {
    match a.to_lowercase().as_ref() {
        "true" => Some(true),
        "false" => Some(false),
        _ => None,
    }
}

fn matchy(tip: &Types) {
    match tip {
        &Types::Antonym => println!("Antonim"),
        &Types::Synonim(ref i) => println!("Sinonim {}", i),
    }
}

fn matchy_2(tip: Types) -> i8 {
    match tip {
        Types::Antonym => 0,
        Types::Synonim(i) => {
            match i.as_ref() {
                "Total" => 2,
                "Partial" => 1,
                _ => -1,
            }
        }

    }
}

impl Entry {
    fn key_add(&self) -> i64 {
        return self.key + 10;
    }
}
