fn main() {
    //let a = "wololo";
    //let c: &str; //would not work from here
    //{
        //let b = String::from("sardanapalus");
        //c = test_life(a, b.as_str());
    //}
    //println!("{}", c);

    let ceva: Life;
    {
        //let _sumthing = String::from("hello"); // it's in the heap...fails...
        //ceva = Life { name: _sumthing.as_str() };
        let _sumthing = "hello"; //Works because it's in rodata or  something
        ceva = Life { name: _sumthing };
    }
    println!("{:#?}", ceva);
}

#[derive(Debug)]
struct Life<'a> {
    name: &'a str,
}

fn test_life<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        return x;
    } else {
        return y;
    }
}
