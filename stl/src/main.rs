use std::collections::HashMap;

fn main() {
    let arr: [i32; 10] = [1, 3, 2, -1, 10, 8, 9, 12, 30, 0];
    let mut vec: Vec<i32> = arr.to_vec();
    let mut count: f32 = 0.0;
    for i in &vec {
        count += *i as f32;
    }
    count /= vec.len() as f32;
    println!("mean is {}", count);

    vec.sort();
    for i in &vec {
        print!("{} ", i);
    }
    println!("");
    let mut median: f32 = 0.0;
    match vec.len() % 2 {
        0 => {
            median = vec[vec.len() / 2] as f32;
            median += vec[vec.len() / 2 - 1] as f32;
            median /= 2.0;
        }
        1 => {
            median = vec[vec.len() / 2] as f32;
        }
        _ => {
            println!("retarded...");
        }
    }
    println!("median is {}", median);
    let mut hashy: HashMap<i32, usize> = HashMap::new();
    for i in &vec {
        let mut sum = hashy.entry(*i).or_insert(0);
        *sum += 1;
    }
    for (k, v) in &hashy {
        println!("{} => {}", k, v);
    }
}
