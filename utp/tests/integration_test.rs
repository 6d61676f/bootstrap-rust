extern crate utp;

use utp::datum;

mod common;

#[test]
#[should_panic]
fn fail_creation() {
    common::setup();
    datum::Square::new(&121);
}
