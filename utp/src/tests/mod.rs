#[cfg(test)]
mod sub_tests;

use super::datum;

#[test]
#[ignore]
fn will_hold() {
    let smaller = datum::Square::new(&5);
    let bigger = datum::Square::new(&10);
    assert!(bigger.can_hold(&smaller));
}

#[test]
//#[should_panic]
//sau
#[should_panic(expected = "fucked up")]
fn will_fail_new() {
    datum::Square::new(&101);
}
