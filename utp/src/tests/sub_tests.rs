use super::datum;

#[test]
#[ignore]
fn will_not_hold() {
    let smaller = datum::Square::new(&5);
    let bigger = datum::Square::new(&10);
    assert!(smaller.can_hold(&bigger));
}
