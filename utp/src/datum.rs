pub struct Square {
    _l: u32,
}

impl Square {
    pub fn can_hold(&self, other: &Square) -> bool {
        return self._l * self._l > other._l * other._l;
    }
    pub fn new(l: &u32) -> Square {
        if *l > 100 {
            panic!("fucked up square...");
        }
        Square { _l: *l }
    }
}
