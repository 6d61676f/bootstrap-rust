use std::marker;

pub struct CacherV2<T, I>
where
    T: Fn(I) -> I, //try and make the function return another type...
    I: Clone,
{
    func: T,
    ret: Option<I>,
}

impl<T, I> CacherV2<T, I>
where
    T: Fn(I) -> I,
    I: Copy,
{
    pub fn new(func: T) -> CacherV2<T, I> {
        CacherV2 {
            func: func,
            ret: None,
        }
    }

    pub fn value(&mut self, arg: I) -> I {
        match self.ret {
            Some(a) => a,
            None => {
                let temp = (self.func)(arg);
                self.ret = Some(temp);
                temp
            }
        }
    }
}

pub struct CacherV3<I, J, T>
where
    T: Fn(I) -> J,
    J: Copy,
{
    func: T,
    ret: Option<J>,
    phantom: marker::PhantomData<I>, //Don't really know what this is,
                                     //but it fails without it, because 'I' appears as unused
}

impl<I, J, T> CacherV3<I, J, T>
where
    T: Fn(I) -> J,
    J: Copy,
{
    pub fn new(func: T) -> CacherV3<I, J, T> {
        CacherV3 {
            func: func,
            ret: None,
            phantom: marker::PhantomData,
        }
    }

    pub fn value(&mut self, arg: I) -> J {
        match self.ret {
            Some(a) => a,
            None => {
                let temp = (self.func)(arg);
                self.ret = Some(temp);
                temp
            }
        }
    }
}
