//Memoization or Lazy evaluation
//
//    FnOnce consumes the variables it captures from its enclosing scope,
//    known as the closure’s environment.
//    To consume the captured variables, the closure must take ownership
//    of these variables and move them into the closure when it is defined.
//    The Once part of the name represents the fact that the closure can’t
//    take ownership of the same variables more than once, so it can be called only once.
//    Fn borrows values from the environment immutably.
//    FnMut can change the environment because it mutably borrows values.
//
extern crate functional;
extern crate rand;

use functional::lazy_cacher::*;
use std::f64::consts;
use rand::Rng;


fn main() {
    println!("{}", do_something(10));

    let mut rng = rand::thread_rng();

    let mut v1: Vec<i32> = Vec::new();
    for _ in 0..10 {
        v1.push(rng.gen());
    }
    let v1_iter = v1.iter();

    let v2: Vec<f64> = v1.iter().map(|x| *x as f64 * consts::PI).collect();
    let v3: Vec<f64> = v2.iter()
        .filter(|s| (**s as f64).floor() as i32 % 2 == 1)
        .map(|x| *x)
        .collect();

    println!("v1");
    for val in v1_iter {
        print!("{} ", val);
    }
    println!("");

    println!("v2");
    for val in v2.iter() {
        print!("{} ", val);
    }
    println!("");
    println!("v3");
    for val in v3.iter() {
        print!("{} ", val);
    }
    println!("");

}

fn do_something(val: i32) -> i32 {
    let mut ceva = CacherV2::new(|x: i32| x * 2 + 1);
    ceva.value(val)
}
