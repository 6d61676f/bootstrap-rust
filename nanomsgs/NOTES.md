### Important

If the endpoint that was created with `bind` ever dies and comes back up the other endpoint
created with `connect` is in an incorrect state and always times out of `poll`ed.

**It seems that the library does not signal you in any manner that the SERVER side has restarted**


Things to try:

 - if `poll` returns success and we try to read/write but it blocks => reconnect
	 - Failed miserably because we could be caught back in `poll`
 - before evey `poll` connect to the other endpoint
	 - **This seems to do the trick**, but I should test it more
