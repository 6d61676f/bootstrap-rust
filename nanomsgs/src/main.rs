extern crate nanomsg;
#[macro_use]
extern crate log;
extern crate env_logger;

use nanomsg::Error as nanoError;
use nanomsg::{Endpoint, PollInOut, PollRequest, Protocol, Socket};
use std::collections::HashMap;
use std::error::Error;

fn main() -> Result<(), Box<Error>> {
    std::env::set_var("RUST_LOG", "nanomsgs");
    env_logger::init();

    let is_daemon = std::env::vars()
        .collect::<HashMap<String, String>>()
        .contains_key("DAEMON");
    let mut sock = Socket::new(Protocol::Pair)?;
    let mut fd_vec = vec![sock.new_pollfd(PollInOut::InOut)];

    let mut end: Endpoint;
    let mut buffer = [0u8; 4096];

    let pid = std::process::id().to_string();

    if is_daemon {
        debug!("Daemon part");
        end = sock.bind("ipc:///tmp/nano.ipc")?;
        let mut poll_req = PollRequest::new(&mut fd_vec[..]);

        loop {
            std::thread::sleep(std::time::Duration::from_secs(2));

            let res = Socket::poll(&mut poll_req, 1000);

            match res {
                Ok(val) => debug!("poll val is {}", val),
                Err(nanoError::TimedOut) => {
                    trace!("timedout");
                    continue;
                }
                Err(e) => {
                    error!("{}", e);
                    break;
                }
            }

            std::thread::sleep(std::time::Duration::from_secs(2));

            let count;
            if poll_req.get_fds()[0].can_read() {
                debug!("Can read...");
                match sock.nb_read(&mut buffer) {
                    Ok(val) => {
                        count = val;
                        debug!("Read {} bytes", count);
                    }
                    Err(nanoError::TryAgain) => {
                        warn!("We've been told that we can READ but it blocks...");
                        continue;
                    }
                    Err(e) => {
                        error!("Error on read {}", e);
                        break;
                    }
                }
            } else {
                count = 0;
                trace!("Could not read");
            }

            std::thread::sleep(std::time::Duration::from_secs(2));

            if poll_req.get_fds()[0].can_write() {
                match sock.nb_write(&mut buffer[..count]) {
                    Ok(count) => info!("Wrote back {} bytes", count),
                    Err(nanoError::TryAgain) => {
                        warn!("We've been told that we can WRITE but it blocks...");
                    }
                    Err(err) => error!("{}", err),
                }
            } else {
                warn!("Could not write back");
            }
        }
        end.shutdown()?;
    } else {
        debug!("Client part");
        loop {
            fd_vec = vec![sock.new_pollfd(PollInOut::InOut)];
            let mut poll_req = PollRequest::new(&mut fd_vec[..]);
            end = sock.connect("ipc:///tmp/nano.ipc")?;
            let res = Socket::poll(&mut poll_req, 1000);

            match res {
                Ok(val) => debug!("poll val is {}", val),
                Err(nanoError::TimedOut) => {
                    trace!("timedout");
                    continue;
                }
                Err(e) => {
                    error!("{}", e);
                    break;
                }
            }

            if poll_req.get_fds()[0].can_write() {
                debug!("Can write...");
                match sock.nb_write(pid.as_bytes()) {
                    Ok(val) => {
                        info!("Wrote {} bytes", val);
                    }
                    Err(nanoError::TryAgain) => {
                        warn!("We've been told that we can WRITE but it blocks...");
                        continue;
                    }
                    Err(e) => {
                        error!("{}", e);
                        break;
                    }
                }
            } else {
                trace!("Could not write");
            }

            if poll_req.get_fds()[0].can_read() {
                match sock.nb_read(&mut buffer) {
                    Ok(val) => {
                        let response = String::from_utf8_lossy(&buffer[..val]);
                        info!("Read back {} bytes {}", val, response);
                    }
                    Err(nanoError::TryAgain) => {
                        warn!("We've been told that we can READ but it blocks...");
                        continue;
                    }
                    Err(e) => {
                        error!("{}", e);
                        break;
                    }
                }
            } else {
                warn!("Could not read back");
            }
            end.shutdown()?;
        }
    }

    Ok(())
}
