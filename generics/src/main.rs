#[derive(Debug)]
struct Ceva<T, U> {
    x: T,
    y: U,
}

impl<T, U> Ceva<T, U> {
    fn get_x(&self) -> &T {
        return &self.x;
    }
    fn get_y(&self) -> &U {
        return &self.y;
    }
}

//or implement on concrete types

impl Ceva<i32, i32> {
    fn mean(&self) -> f32 {
        return (self.x as f32 + self.y as f32) / 2.0;
    }
}

//or partially concrete types

impl<T> Ceva<T, i32> {
    fn some_func(&self) -> () {
        println!("carry on....");
    }
}

#[derive(Debug)]
struct Point<T, U> {
    x: T,
    y: U,
}

//Trait Bounds
impl<T, U> Point<T, U>
where
    T: Clone,
    U: Clone,
{
    fn mixup<V, W>(&self, other: &Point<V, W>) -> Point<T, W>
    where
        V: Clone,
        W: Clone,
    {
        Point {
            x: self.x.clone(),
            y: other.y.clone(),
        }
    }
}

fn main() {
    let a = Ceva { x: 5, y: 'c' };
    let p1 = Point { x: 5, y: 'c' };
    let p2 = Point { x: "sal", y: 5.5 };
    let p3 = p1.mixup(&p2);
    println!("p1 is {:#?}", p1);
    println!("p2 is {:#?}", p2);
    println!("p3 is {:#?}", p3);
    println!("a is {:#?}", a);
    println!("a is {:#?}", a);
    println!("a.x is {}, a.y is {}", a.get_x(), a.get_y());
}
